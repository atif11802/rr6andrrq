import Navbar from "./components/Navbar";
import { Routes, Route } from "react-router-dom";
import { ReactQueryDevtools } from "react-query/devtools";
import Users from "./components/Users";
import "./App.css";
import Posts from "./components/Posts";
import Home from "./components/Home";

function App() {
	return (
		<div className='app'>
			<Navbar />

			<Routes>
				<Route path='/' element={<Home />} />
				<Route path='/posts' element={<Posts />} />
				<Route path='/users' element={<Users />} />
			</Routes>
			<ReactQueryDevtools initialIsOpen={false} />
		</div>
	);
}

export default App;
