import React from "react";
import "../styles/person.css";

const Person = ({ user }) => {
	// console.log(user);
	return (
		<div className='person'>
			<h4>name: {user.name}</h4>
			<p>Gender: {user.gender}</p>
			<p>email: {user.email}</p>
		</div>
	);
};

export default Person;
