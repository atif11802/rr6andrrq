import React from "react";
import { NavLink } from "react-router-dom";
import "../styles/navbar.css";

const Navbar = () => {
	return (
		<div className='navbar'>
			<NavLink to='/posts'>posts</NavLink> <NavLink to='/users'>Users</NavLink>
		</div>
	);
};

export default Navbar;
