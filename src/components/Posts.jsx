import React from "react";
import { useQuery } from "react-query";
import axios from "axios";
import Post from "./Post";

const fetchPosts = async () => {
	try {
		let res = await axios.get("https://gorest.co.in/public/v1/posts");
		return res.data;
	} catch (err) {
		console.log(err);
	}
};

const Posts = () => {
	const { data, status } = useQuery("Posts", fetchPosts);
	// console.log(data?.data);
	return (
		<div>
			<h2>posts</h2>
			{status === "loading" && <p>loading...</p>}
			{status === "error" && <p>error fetching data</p>}
			{status === "success" && (
				<ul>
					{data?.data.map((post, ind) => {
						return <Post key={ind} post={post} />;
					})}
				</ul>
			)}
		</div>
	);
};

export default Posts;
