import axios from "axios";
import React from "react";
import { useQuery } from "react-query";
import "../styles/users.css";
import Person from "./Person";

const fetchUsers = async () => {
	try {
		let res = await axios.get("https://gorest .co.in/public/v1/users");
		return res.data;
	} catch (err) {
		throw new err("Something went worng " + err.message);
		console.log(err);
		// return err;
	}
};

const Users = () => {
	const { data, status, error } = useQuery("Users", fetchUsers);
	console.log(status, error);

	return (
		<div>
			<h2>Users</h2>
			{status === "loading" && <p>loading...</p>}
			{status === "error" && <p>error fetching data</p>}
			{status === "success" && (
				<ul>
					{data?.data?.map((user, ind) => {
						return <Person key={ind} user={user} />;
					})}
				</ul>
			)}
		</div>
	);
};

export default Users;
